"""
   Auxiliary web functions for the demos.

   Author: Mar Alguacil
"""

import io
import cv2
import logging
import numpy as np
# import os
# os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import tensorflow as tf
from trainer_tester.tester_one import TesterOne
from trainer_tester.options import parse_args
from PIL import Image


tf.get_logger().setLevel(logging.ERROR)

gpus = tf.config.list_physical_devices('GPU')
if len(gpus)>0:
    tf.config.experimental.set_memory_growth(gpus[0], True)

# Parse arguments
args = parse_args(train=False, one=True)
args.semantic_label_path = './labels/semantic_labels.txt'
args.checkpoint_dir = './checkpoints'
args.use_vae = True
# Initialize tester
tester = TesterOne(args)

def generateFake(segmap, styimg):
    """Creates a fake image from the segmentation map and the style image, and
      returns a file-object in memory with the synthesized image.
    """
    # Create synthesized image
    fake_img = tester.generate_fake(segmap, styimg)[0].numpy()

    # Denormalize
    fake_img = (fake_img + 1) * 127.5

    # Convert numpy array of type float64 to type uint8
    fake_img = fake_img.astype(np.uint8)

    # Credit: https://stackoverflow.com/questions/56946969/how-to-send-an-image-directly-from-flask-server-to-html
    # Convert numpy array to PIL Image
    fake_img = Image.fromarray(fake_img)

    # Create file-object in memory
    file_object = io.BytesIO()

    # Write PNG in file-object
    fake_img.save(file_object, 'PNG')

    # Move to beginning of file so `send_file()` it will read from start
    file_object.seek(0)

    return file_object

def file2img(file, color_mode=cv2.IMREAD_COLOR):
    """Converts an image file into an OpenCV image.
    Main credit: https://stackoverflow.com/questions/47515243/reading-image-file-file-storage-object-using-cv2
    """
    # Read image file string data
    img = file.read()

    # Convert string data to numpy array
    img = np.frombuffer(img, np.uint8)

    # Convert numpy array to image
    img = cv2.imdecode(img, color_mode)

    if color_mode == cv2.IMREAD_COLOR:
        # OpenCV likes to treat images as BGR instead of RGB. We need to swap layers
        #https://docs.opencv.org/3.4/d4/da8/group__imgcodecs.html#gga61d9b0126a3e57d9277ac48327799c80aeddd67043ed0df14f9d9a4e66d2b0708
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    return img