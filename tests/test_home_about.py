"""
    Home and About Tests.

    Author: Mar Alguacil
"""
from basic import TestBasic

class TestHomeAbout(TestBasic):
    def test_home(self):
        """Tests Home view display."""
        result = self.app.get('/')
        self.check(result.status_code, 200,
                   'Home', "Page successfully rendered.")

    def test_about(self):
        """Tests About view display."""
        result = self.app.get('/about')
        self.check(result.status_code, 200,
                   'About', "Page successfully rendered.")

