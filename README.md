[![Status](https://img.shields.io/badge/Status-InProgress-green.svg)](https://bitbucket.org/MarAl15/dibphot/src/master/README.md)
[![Language](https://img.shields.io/badge/Language-Python3.8-blue.svg)](https://www.python.org/)

# DibPhot - Semantic Image Synthesis

Photorealistic images creation from semantic segmentation masks, which are labeled sketches that depict the layout of a scene.
