/*
 *  Draw & Fill
 *
 *  Author: Mar Alguacil
 */


/*
 * Fills a geometric shape.
 */
function fillShape(x, y, edge_color, edge_pixels) {
    let open_list = [{x: x, y: y}],
        checked = Array(ctx.canvas.width).fill().map(()=>Array(ctx.canvas.height).fill(false));

    const red = (open_list[0].y * ctx.canvas.width + open_list[0].x) * 4,
          new_color = hex2rgb(ctx.fillStyle);

    // Mark the shape edges as checked
    const canvas_width = ctx.canvas.width*4;
    for (pixel_red of edge_pixels) {
        const x = (pixel_red%canvas_width)/4,
              y = parseInt(pixel_red/canvas_width);

        checked[x][y] = true;
    }
    // While the open list is not empty
    while (open_list.length > 0) {
        // Get element off the OPEN list
        let pixel = open_list.pop();

        // Continue to colour and check the neighbours of this pixel if it was
        // not checked previously, and has a different colour from the end
        // or has the same colour but does not belong to the shape edge.
        if (!checked[pixel.x][pixel.y]){
            // Compute red colour position of the current element
            const r = (pixel.y * ctx.canvas.width + pixel.x) * 4;

            // Colour the pixel whether it has a different colour than the edge_color
            //~ if (!colorMatch(r, edge_color))
                setPixelColor(r, new_color);

            // Mark pixel as checked
            checked[pixel.x][pixel.y] = true;

            // Add the neighbouring pixel whether it is in the canvas and was not checked
            if (pixel.x>0 && !checked[pixel.x-1][pixel.y])
                open_list.push({x: (pixel.x-1), y: pixel.y});

            if (pixel.x<ctx.canvas.width-1 && !checked[pixel.x+1][pixel.y])
                open_list.push({x: (pixel.x+1), y: pixel.y});

            if (pixel.y>0 && !checked[pixel.x][pixel.y-1])
                open_list.push({x: pixel.x, y: (pixel.y-1)});

            if (pixel.y<ctx.canvas.height-1 && !checked[pixel.x][pixel.y+1])
                open_list.push({x: pixel.x, y: (pixel.y+1)});
        }
    }

    // Update canvas
    ctx.putImageData(img, 0, 0);
}


/*
 * Draws a line using Bresenham's line algorithm.
 */
const drawLine = (x0, y0, x1, y1, new_color, perpendicular=false) => {
    const dx = Math.abs(x1-x0), sx = x0 < x1 ? 1 : -1,
          dy = Math.abs(y1-y0), sy = y0 < y1 ? 1 : -1,
          len = perpendicular ? (ctx.lineWidth+1)/2 : -1;

    let x=x0, y = y0,
        D, D0_inc, D1_inc,
        i = 0;

    if (dx > dy) {
        D = 2 * dy - dx;
        D0_inc = 2 * dy;
        D1_inc = 2 * (dy - dx);
    }
    else {
        D = 2 * dx - dy;
        D0_inc = 2 * dx;
        D1_inc = 2 * (dx - dy);
    }

    while (((!perpendicular && (x != x1 || y != y1)) || ++i<len)){
        setPixelColorXY(x, y, new_color);

        if (D>=0) {
            y += sy; x += sx;
            D += D1_inc;
        }
        else {
            if (dx > dy) x += sx;
            else y += sy;
            D += D0_inc;
        }
    }

    if (!perpendicular)
        setPixelColorXY(x1, y1, new_color);

    return {x: x, y: y};
}

/*
 *  Plot a (thick) line.
 */
function plotLine(x0, y0, x1, y1) {
    img = ctx.getImageData(0, 0, ctx.canvas.width, ctx.canvas.height);
    const new_color = hex2rgb(ctx.strokeStyle),
          line_pixels = [],
          pixel_colors = [];

    if (ctx.lineWidth == 1) {
        // Save pixels
        savePixel = (red_index) => {
            const color = img.data.slice(red_index, red_index+3);
            pixel_colors.push({r: color[0], g: color[1], b: color[2]});
            line_pixels.push(red_index);
            return true;
        }

        // Draw line
        drawLine(x0, y0, x1, y1, new_color);

        // Update canvas
        ctx.putImageData(img, 0, 0);
    }
    else {
        savePixel = (red_index) => {
            if (!line_pixels.includes(red_index)) {
                const color = img.data.slice(red_index, red_index+3);
                pixel_colors.push({r: color[0], g: color[1], b: color[2]});
                line_pixels.push(red_index);
                return true;
            }
            return false;
        }

        /* Draw line shape*/
        const dx = x1-x0,
              dy = y1-y0;

        // Normal lines
        const pixel2 = drawLine(x0, y0, x0 + dy, y0 - dx, new_color, perpendicular=true),
              pixel3 = drawLine(x0, y0, x0 - dy, y0 + dx, new_color, perpendicular=true),
              pixel4 = drawLine(x1, y1, x1 + dy, y1 - dx, new_color, perpendicular=true),
              pixel5 = drawLine(x1, y1, x1 - dy, y1 + dx, new_color, perpendicular=true);

        drawLine(pixel2.x, pixel2.y, pixel4.x, pixel4.y, new_color);
        drawLine(pixel3.x, pixel3.y, pixel5.x, pixel5.y, new_color);

        /* Fill line */
        // Do not check if the pixel was saved previously
        savePixel = (red_index) => {
            const color = img.data.slice(red_index, red_index+3);
            pixel_colors.push({r: color[0], g: color[1], b: color[2]});
            line_pixels.push(red_index);
            return true;
        }

        fillShape(Math.round((x0+x1)/2), Math.round((y0+y1)/2),
                  new_color, line_pixels);
    }

    return { pixels: line_pixels,
             previous_color: pixel_colors,
             color: ctx.strokeStyle }
}


/*
 *  Plot a rectangle.
 */
function plotRectangle(x0, y0, width, height) {
    img = ctx.getImageData(0, 0, ctx.canvas.width, ctx.canvas.height);
    const new_color = hex2rgb(ctx.strokeStyle),
          si = width > 0 ? 1 : -1,
          sj = height > 0 ? 1 : -1,
          rectangle_pixels = [],
          pixel_colors = [];

    // Save concerned pixels
    savePixel = (red_index) => {
        const color = img.data.slice(red_index, red_index+3);
        pixel_colors.push({r: color[0], g: color[1], b: color[2]});
        rectangle_pixels.push(red_index);
        return true;
    }

    // Draw rectangle
    for (let i=0; i!=width+si; i+=si)
        for (let j=0; j!=height+sj; j+=sj)
            setPixelColorXY(x0+i, y0+j, new_color);

    // Update canvas
    ctx.putImageData(img, 0, 0);

    return { pixels: rectangle_pixels,
             previous_color: pixel_colors,
             color: ctx.strokeStyle }
}


/*
 *  Plot a circle using Bresenham's circle algorithm.
 */
function plotCircle(x_center, y_center, radius) {
    img = ctx.getImageData(0, 0, ctx.canvas.width, ctx.canvas.height);
    const new_color = hex2rgb(ctx.strokeStyle),
          circle_pixels = [],
          pixel_colors = [];

    // Save concerned pixels
    savePixel = (red_index) => {
        const color = img.data.slice(red_index, red_index+3);
        pixel_colors.push({r: color[0], g: color[1], b: color[2]});
        circle_pixels.push(red_index);
        return true;
    }

    // Function for drawing the new pixel and all other 7 pixels present at symmetric position
    const drawSymmetricPoints = (sx, sy) => {
        setPixelColorXY(x_center+sx, y_center+sy, new_color);
        setPixelColorXY(x_center-sx, y_center+sy, new_color);
        setPixelColorXY(x_center+sx, y_center-sy, new_color);
        setPixelColorXY(x_center-sx, y_center-sy, new_color);
        setPixelColorXY(x_center+sy, y_center+sx, new_color);
        setPixelColorXY(x_center-sy, y_center+sx, new_color);
        setPixelColorXY(x_center+sy, y_center-sx, new_color);
        setPixelColorXY(x_center-sy, y_center-sx, new_color);
    }

    /* Draw circle */
    let sx = 0, sy = radius,
        D = 3 - 2 * radius;
    drawSymmetricPoints(sx, sy);

    while (sy >= sx) {
        // For each pixel, draw all eight pixels
        ++sx;

        // Check for decision parameter and correspondingly
        // update D, sx, sy
        if (D > 0){
            --sy;
            D += 4 * (sx - sy) + 10;
        }
        else
            D += 4 * sx + 6;
        drawSymmetricPoints(sx, sy);
    }

    /* Fill circle */
    fillShape(x_center, y_center,
              hex2rgb(ctx.strokeStyle), circle_pixels);

    return { pixels: circle_pixels,
             previous_color: pixel_colors,
             color: ctx.strokeStyle }
}


/*
 *  Plot a rhombus.
 */
function plotRhombus(x_center, y_center, x1_corner, y1_corner) {
    img = ctx.getImageData(0, 0, ctx.canvas.width, ctx.canvas.height);
    const new_color = hex2rgb(ctx.strokeStyle),
          dx = x1_corner - x_center, dy = y1_corner - y_center,
          rhombus_pixels = [],
          pixel_colors = [];

    /* Draw rhombus shape*/
    // Save pixels after checking
    savePixel = (red_index) => {
        if (!rhombus_pixels.includes(red_index)) {
            const color = img.data.slice(red_index, red_index+3);
            pixel_colors.push({r: color[0], g: color[1], b: color[2]});
            rhombus_pixels.push(red_index);
            return true;
        }
        return false;
    }

    const x2_corner = x_center - dy, y2_corner = y_center + dx,
          x3_corner = x_center - dx, y3_corner = y_center - dy,
          x4_corner = x_center + dy, y4_corner = y_center - dx;
    drawLine(x1_corner, y1_corner, x2_corner, y2_corner, new_color);
    drawLine(x2_corner, y2_corner, x3_corner, y3_corner, new_color);
    drawLine(x3_corner, y3_corner, x4_corner, y4_corner, new_color);
    drawLine(x4_corner, y4_corner, x1_corner, y1_corner, new_color);

    /* Fill rhombus */
    // Save pixels without checking
    savePixel = (red_index) => {
        const color = img.data.slice(red_index, red_index+3);
        pixel_colors.push({r: color[0], g: color[1], b: color[2]});
        rhombus_pixels.push(red_index);
        return true;
    }

    fillShape(x_center, y_center,
              hex2rgb(ctx.strokeStyle), rhombus_pixels);

    return { pixels: rhombus_pixels,
             previous_color: pixel_colors,
             color: ctx.strokeStyle }
}


/*
 * Fills selected area with a colour.
 */
function fill(x, y){
    img = ctx.getImageData(0, 0, ctx.canvas.width, ctx.canvas.height);

    const red = (y * ctx.canvas.width + x) * 4,
          color2change = {r: img.data[red], g: img.data[red+1], b: img.data[red+2]},
          new_color = hex2rgb(ctx.fillStyle),
          canvas_width = ctx.canvas.width * 4;

    let checked = Array(ctx.canvas.width).fill().map(()=>Array(ctx.canvas.height).fill(false)),
        open_list = [{x: x, y: y}];

    const filled_pixels = [],
          pixel_colors = [];

    // Save changed pixels
    savePixel = (red_index) => {
        const color = img.data.slice(red_index, red_index+3);
        pixel_colors.push({r: color[0], g: color[1], b: color[2]});
        filled_pixels.push(red_index);
        return true;
    }

    // While the open list is not empty
    while (open_list.length > 0) {
        // Get element off the OPEN list
        pixel = open_list.pop();

        // Continue to colour and check the neighbours of this pixel if it was not previously checked
        if (!checked[pixel.x][pixel.y]) {
            // Compute red colour position of the current element
            const r = (pixel.y * ctx.canvas.width + pixel.x) * 4;

            // Colour pixel
            setPixelColor(r, new_color);

            // Mark pixel as checked
            checked[pixel.x][pixel.y] = true;

            //  Add neighboring pixel whether it is in the canvas, was not previously checked and
            // still belongs to the same area.
            if (pixel.x>0 && !checked[pixel.x-1][pixel.y] &&
                colorMatch(r-4, color2change))
                open_list.push({x: (pixel.x-1), y: pixel.y});

            if (pixel.x<ctx.canvas.width-1 && !checked[pixel.x+1][pixel.y] &&
                colorMatch(r+4, color2change))
                open_list.push({x: (pixel.x+1), y: pixel.y});

            if (pixel.y>0 && !checked[pixel.x][pixel.y-1] &&
                colorMatch(r-canvas_width, color2change))
                open_list.push({x: pixel.x, y: (pixel.y-1)});

            if (pixel.y<ctx.canvas.height-1 && !checked[pixel.x][pixel.y+1] &&
                colorMatch(r+canvas_width, color2change))
                open_list.push({x: pixel.x, y: (pixel.y+1)});

        }
    }

    // Update canvas
    ctx.putImageData(img, 0, 0);

    return { pixels: filled_pixels,
             previous_color: pixel_colors,
             color: ctx.strokeStyle
           }
}

/*
 * Draws brush tip shape from the given plotting fuction.
 */
function drawonmousedown(x, y, plot_function) {
    const half_size = Math.round((ctx.lineWidth+1)/2)
    if (x-half_size >= 0 && x+half_size < ctx.canvas.width &&
        y-half_size >= 0 && y+half_size < ctx.canvas.height) {
        // Create brush tip shape
        const change = plot_function(x, y, half_size);

        // Save pixels and colours
        pixels = pixels.concat(change.pixels);
        previous_colors = previous_colors.concat(change.previous_color);

        // Return pixels of the brush tip shape
        return change.pixels
    }

    const shape_pixels = [];

    // Save pixels, but do not colour them
    setPixelColorXY = (x_, y_, color) => {
        const r_ = (x_ + y_ * img.width) * 4;
        shape_pixels.push(r_)
        savePixel(r_) // To calculate the filling afterwards
    }
    setPixelColor = (r_, color) => {
        shape_pixels.push(r_)
    }

    // Create brush tip shape in the center of the canvas
    const x_center = Math.round(ctx.canvas.width/2),
          y_center = Math.round(ctx.canvas.height/2)
    plot_function(x_center, y_center, half_size);

    // Return to normal functions for colouring pixels
    setPixelColorXY = utils.setPixelColorXY;
    setPixelColor = utils.setPixelColor;

    /* Translate brush tip shape to the right position */
    const dx = (x - x_center) * 4,
          dy = (y - y_center) * img.width * 4,
          new_color = hex2rgb(ctx.strokeStyle),
          changed_colors = [],
          changed_pixels = [],
          brush_tip_shape = [];
    let checked = Array(img.width*img.height).fill(true);

    // Limit where to draw
    const half = Math.round((ctx.lineWidth+1)/2) + 2;
    for (let xi=x-half_size-2; xi<x+half_size+2; ++xi)
        for (let yi=y-half_size-2; yi<y+half_size+2; ++yi)
            if (xi >= 0 && xi < img.width && yi >= 0 && yi < img.height)
                checked[yi * img.width + xi] = false;

    // Save all pixels in the brush_tip_shape list,
    // pixels to draw in changed_colors and their colours in changed_colors
    savePixel = (red_index) => {
        brush_tip_shape.push(red_index);

        if (!checked[red_index/4]) {
            const color = img.data.slice(red_index, red_index+3);
            changed_colors.push({r: color[0], g: color[1], b: color[2]});
            changed_pixels.push(red_index);
            return true;
        }
        return false;
    }

    // Colour translated pixels
    for (let r of shape_pixels)
        setPixelColor(r+dx+dy, new_color);

    // Update canvas
    ctx.putImageData(img, 0, 0);

    // Save changed pixels
    pixels = pixels.concat(changed_pixels);
    previous_colors = previous_colors.concat(changed_colors);

    // Return pixels of the brush tip shape
    return brush_tip_shape;
}

/*
 * Draws a line with the given brush.
 */
function plotLineShape(x0, y0, x1, y1, brush_tip_shape) {
    img = ctx.getImageData(0, 0, ctx.canvas.width, ctx.canvas.height);
    const dx = Math.abs(x1-x0), sx = x0 < x1 ? 1 : -1,
          dy = Math.abs(y1-y0), sy = y0 < y1 ? 1 : -1,
          srx = sx*4,
          sry = sy*ctx.canvas.width*4,
          new_color = hex2rgb(ctx.strokeStyle);

    let x = x0, y = y0,
        ix = 0, iy = 0,
        D, D0_inc, D1_inc,
        checked = Array(ctx.canvas.width*ctx.canvas.height).fill(true);

    // Limit where to draw
    const half = Math.round((ctx.lineWidth+1)/2) + 2;
    for (let xi=x0-sx*half; xi!=x1+sx*half; xi+=sx)
        for (let yi=y0-sy*half; yi!=y1+sy*half; yi+=sy)
            if (xi >= 0 && xi < img.width && yi >= 0 && yi < img.height)
                checked[yi * ctx.canvas.width + xi] = false;

    for (let r of brush_tip_shape)
        checked[r/4] = true;

    const line_shape_pixels = [],
          pixel_colors = [];

    // Save changed pixels
    savePixel = (red_index) => {
        const checked_index = red_index/4;
        if (!checked[checked_index]) {
            const color = img.data.slice(red_index, red_index+3);
            pixel_colors.push({r: color[0], g: color[1], b: color[2]});
            line_shape_pixels.push(red_index);
            checked[checked_index] = true;
            return true;
        }
        return false;
    }

    // Bresenham's line algorithm
    if (dx > dy) {
        D = 2 * dy - dx;
        D0_inc = 2 * dy;
        D1_inc = 2 * (dy - dx);
    }
    else {
        D = 2 * dx - dy;
        D0_inc = 2 * dx;
        D1_inc = 2 * (dx - dy);
    }
    while (x != x1 || y != y1) {
        if (D>=0) {
            y += sy; x += sx;
            iy =sry; ix =srx;
            D += D1_inc;
        }
        else {
            if (dx > dy) {
                x += sx;
                ix = srx; iy = 0;
            }
            else {
                y += sy;
                iy = sry; ix = 0;
            }
            D += D0_inc;
        }

        // Move and colour pixels
        for (let i=0; i<brush_tip_shape.length; ++i) {
            brush_tip_shape[i] += ix + iy;
            setPixelColor(brush_tip_shape[i], new_color);
        }
    }

    // Update canvas
    ctx.putImageData(img, 0, 0);

    return { pixels: line_shape_pixels,
             previous_color: pixel_colors,
             color: ctx.strokeStyle
           }
}